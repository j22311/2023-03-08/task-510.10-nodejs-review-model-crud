const express = require("express"); // Tương tự : import express from "express";

const { drinkClassList, drinkObjectList } = require("./data"); // Tương tự: import {data} from "./data";

// Khởi tạo Express App
const app = express();

const port = 8000;

// Cấu hình để API đọc được body JSON
app.use(express.json());

// Cấu hình để API đọc được body có ký tự tiếng Việt
app.use(express.urlencoded({
    extended: true
}))

// Khai báo APi dạng Get "/" sẽ chạy vào đây
app.get("/", (request, response) => {
    response.status(200).json({
        message: "Drinks API"
    })
})

app.get("/drink-class", (request, response) => {
    let code = request.query.code;

    if(code) {
        // Khởi tạo biến drinkListResponse là kết quả tìm được 
        let drinkListResponse = [];

        // Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
        for (let index = 0; index < drinkClassList.length; index++) {
            let drinkElement = drinkClassList[index];

            if(drinkElement.checkDrinkByCode(code)) {
                drinkListResponse.push(drinkElement);
            }
        };

        response.status(200).json({
            drinks: drinkListResponse
        })
    } else {
        response.status(200).json({
            drinks: drinkClassList
        })
    }
})

app.get("/drink-class/:drinkId", (request, response) => {
    let drinkId = request.params.drinkId;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    drinkId = parseInt(drinkId); 

    // Khởi tạo biến drinkResponse là kết quả tìm được 
    let drinkResponse = null;

    // Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
    for (let index = 0; index < drinkClassList.length; index++) {
        let drinkElement = drinkClassList[index];

        if(drinkElement.checkDrinkById(drinkId)) {
            drinkResponse = drinkElement;
            // Nếu tìm được thì thoát khỏi vòng lặp ngay lập tức
            break;
        }
    };

    response.status(200).json({
        drinks: drinkResponse
    })
})

app.get("/drink-object", (request, response) => {
    let code = request.query.code;

    if(code) {
        // Viết hoa code nhập vào
        code = code.toUpperCase();

        // Khởi tạo biến drinkListResponse là kết quả tìm được 
        let drinkListResponse = [];

        // Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
        for (let index = 0; index < drinkObjectList.length; index++) {
            let drinkElement = drinkObjectList[index];

            if(drinkElement.code.includes(code)) {
                drinkListResponse.push(drinkElement);
            }
        };

        response.status(200).json({
            drinks: drinkListResponse
        })
    } else {
        response.status(200).json({
            drinks: drinkObjectList
        })
    }
})

app.get("/drink-object/:drinkId", (request, response) => {
    let drinkId = request.params.drinkId;
    // Do drinkId nhận được từ params là string nên cần chuyển thành kiểu integer
    drinkId = parseInt(drinkId); 

    // Khởi tạo biến drinkResponse là kết quả tìm được 
    let drinkResponse = null;

    // Duyệt từng phần tử của mảng để tìm được đồ uống có Id tương ứng
    for (let index = 0; index < drinkClassList.length; index++) {
        let drinkElement = drinkClassList[index];

        if(drinkElement.id  === drinkId) {
            drinkResponse = drinkElement;
            // Nếu tìm được thì thoát khỏi vòng lặp ngay lập tức
            break;
        }
    };

    response.status(200).json({
        drinks: drinkResponse
    })
})

app.listen(port, () => {
    console.log(`App Listening on port ${port}`);
})
